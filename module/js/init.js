window.onload = function(){
    PatternsLoad("all");
}

function openButton(){
    document.getElementById("modal").style.display = "block";
    document.getElementById("modal-overlay").style.display = "block";
}

function closeButton(){
document.getElementById("modal").style.display = "none";
document.getElementById("modal-overlay").style.display = "none";
}

function PatternsLoad(name){

}

function PatternLoadForWork(name){

}

function PatternSave(){
    var pattern_name = document.getElementById("pattern_name").value;
    var pattern_description = document.getElementById("pattern_description").value;
    var inputs_count = pattern_description.split("#*").length - 1;
    console.log('Дебаг '+pattern_name+" | " + pattern_description + " | " + inputs_count);
    $.ajax({
        type: 'POST', // метод
        url: 'module/php/PatternSave.php', // скрипт который получит отправление
        //dataType: 'json', // тип послания
        data: { // тут создаем json со значением которое нужно
            pattern_name:pattern_name,
            pattern_description:pattern_description,
            inputs_count:inputs_count
        },
        success: function (data) { // необязательная функция, срабатывает при успехе
          PatternLoadForWork(pattern_name);
          console.log(data);
    
        },
        error: function () {// необязательная функция, срабатывает при неудаче
         alert('Error');
        }
    }); 
    
    PatternsLoad("all");
}